import { useState } from "react";

function Todo(props) {
  const { id, title, completed } = props;
  console.log(props);
  const [done, setDone] = useState(completed);
  return (
    <div className="Todo">
      <button
        onClick={() => {
          setDone(!done);
        }}
      >
        {done == true ? "X" : "V"}
      </button>
      <h1>{title}</h1>
    </div>
  );
}

export default Todo;
