import React from "react";
import { useState } from "react";
import "./App.css";
import Todo from "./components/Todo";
import axios from "axios";

const API_URL = "https://jsonplaceholder.typicode.com/todos/";

// const todolist = [
//   {
//     id: "1",
//     title: "Acheter du pain",
//     completed: false,
//   },
//   {
//     id: "2",
//     title: "Faire des brief",
//     completed: true,
//   },
//   {
//     id: "3",
//     title: "Travailler sur le projet",
//     completed: false,
//   },
// ];

function App() {
  const [todos, setTodos] = useState([]);
  function loadTodos() {

    axios.get(API_URL).then((response) => {
      setTodos(response.data);
      console.log(response.data);
    });
  };
  return (
    <div className="App">
      <div>
        {todos.map((element) => {
          return <Todo key={element.id} {...element} />;
        })}
      </div>
      <button onClick={loadTodos}>Show all</button>
      <button onClick={loadTodos}>Show to do</button>
      <button onClick={loadTodos}>Show done</button>
      
    </div>
  );
}
export default App;
