Tâche	Ressources		
"Initialiser une application React nommée todo-list avec vite"
(la commande d'initialisation va te guider, puis te montrer comment lancer ton projet)"	https://vitejs.dev/guide/#scaffolding-your-first-vite-project		
Exécuter, tester puis arrêter une application React avec npm	https://nodejs.dev/learn/how-to-exit-from-a-nodejs-program#:~:text=When%20running%20a%20program%20in%20the%20console%20you%20can%20close%20it%20with%20ctrl-C,%20but%20what%20we%20want%20to%20discuss%20here%20is%20programmatically%20exiting.		
Ouvrir plusieurs onglet de terminal dans Visual Studio Code pour garder un onglet avec le server React exécuté et un autre onglet pour utiliser git	https://code.visualstudio.com/docs/editor/integrated-terminal#:~:text=You%20can%20create%20multiple%20terminals%20open%20to%20different%20locations%20and%20easily%20navigate%20between%20them.%20Terminal%20instances%20can%20be%20added%20by%20clicking%20the%20plus%20icon%20on%20the%20top-right%20of%20the%20TERMINAL%20panel%20or%20by%20triggering%20the%20Ctrl+Shift+%60%20command.%20This%20action%20creates%20another%20entry%20in%20the%20dropdown%20list%20that%20can%20be%20used%20to%20switch%20between%20them.		
Connaître la structure d'un projet React	https://www.freecodecamp.org/news/quick-guide-to-understanding-and-creating-reactjs-apps-8457ee8f7123/#:~:text=When%20you%20created%20the%20project,%20you%20would%20have%20noticed%20that%20it%20created%20a%20bunch%20of%20files.%20Here%20I%20will%20list%20out%20some%20of%20the%20important%20files%20and%20folders%20that%20you%20should%20be%20aware%20of%20.		
"Ouvrir le fichier src/App.js et étudier le contenu du ""return"".
Remplacer le texte de la balise <p> et observer le changement lors de l'exécution de l'application. "	https://fr.reactjs.org/docs/introducing-jsx.html#gatsby-focus-wrapper		
"Le fichier src/App.js correspond à un composant App.
Connaître la structure d'un composant fonctionnel."	https://fr.accentsconagua.com/articles/code/stateful-vs-stateless-functional-components-in-react.html		
"Dans le composant App, supprimer le contenu de la DIV dont la classe est ""App""
(il faut garder la DIV, ne supprimer que ce quelle contient).

Ajouter le JSX suivant dans la DIV : 
- Une balise DIV avec une classe Todo
- A l'intérieur de cette dernière, une balise P avec le texte de votre choix
Observer le changement lors de l'exécution de l'application. "	https://fr.reactjs.org/docs/jsx-in-depth.html		
"Créer le dossier src/components et créer un composant Todo.jsx, dont le contenu est le suivant : 
- Une balise DIV avec une classe Todo
- A l'intérieur de cette dernière, une balise P avec le texte de votre choix (différent du texte de l'étape précédente)
Exporter le composant Todo."	https://fr.reactjs.org/docs/components-and-props.html		
"Importer le composant Todo dans App.
Appeler le composant Todo dans le JSX du componsant App : à l'intérieur de la DIV de classe ""App"", mais à l'extérieur de la DIV de classe Todo déjà présente."			
"Dans le composant App, supprimer la DIV de classe Todo.
Appelez une seconde fois le composant Todo."			
"Utiliser les props pour envoyer un texte différent dans chaque appel du composant Todo, dans une propriété ""title"".
Récupérer les props dans le composant Todo.
Remplacer le texte en dur de la balise P par la propriété ""title"" des props."			
"Les props sont des objets dont chaque propriété est envoyée lors de l'appel du composant par le parent.
Dans le composant Todo, au début de la fonction faire un console.log(props) et observer la structure de l'objet.
Déstructurer la propriété ""title"" des props et utiliser la variable obtenue dans le JSX."	https://mindsers.blog/fr/decomposition-et-destructuration-en-javascript/#:~:text=let%20{%20valeur1,%20valeur2%20}%20=%20objet		
"Dans du JSX, il est possible d'appeler des expressions mais pas des instructions (statements).
Se renseigner sur la différence entre les deux."	https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Expressions_and_Operators		
	https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Statements		
Se renseigner sur les opérateurs ternaires (opérateur conditionnel)	https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Conditional_Operator		
"Dans le composant App, lors de l'appel des composants Todo, envoyer une propriété booléenne ""completed"" avec la valeur false.
Dans le composant Todo, modifiez la déstructuration des props afin de récupérer la propriété ""completed""."	https://arcticicestudio.github.io/styleguide-javascript/rules/react/props.html#boolean-attributes-notation		
"Dans le composant Todo, ajouter une balise P dans la DIV Todo.
Si la propriété ""completed"" des props est vraie, afficher le texte ""Done"", sinon afficher le texte ""Todo""."	https://fr.reactjs.org/docs/conditional-rendering.html#inline-if-else-with-conditional-operator		
Se renseigner sur l'écriture d'un objet littéral.	https://yard.onl/sitelycee/cours/js/_Js.html?Lesobjetslittraux.html		
Dans le composant App, au-dessus de la fonction, déclarer un tableau todoList contenant trois objets ayant pour propriété "id" qui est un nombre, "title" qui est une chaîne de caractères et "completed" qui est un booléen (avec des valeurs aux choix. pour chaque objet)	https://developer.mozilla.org/fr/docs/Learn/JavaScript/First_steps/Arrays#:~:text=let%20shopping%20=		
Se renseigner sur l'écriture des "map"	https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/map		
Dans le componsant App, remplacer le contenu de la DIV de classe "App" par l'utilisation d'un itérateur "map", afin d'afficher des composants Todo.	https://fr.reactjs.org/docs/lists-and-keys.html		
Modifier la façon dont le composant Todo est retourné dans le map pour y ajouter les props "title" et "completed".			
"Lors de l'exécution de l'application, ouvrir la console du navigateur et regarder le message d'erreur.
Lors de l'appel du composant Todo dans le map, ajouter la propriété ""key"" avec la valeur de ""id"" correspondante."			
"Nous allons améliorer l'application afin de pouvoir modifier l'état ""Done"" ou ""Todo"" d'une tâche à l'aide d'un bouton. Actuellement l'application se base sur la props ""completed"" pour afficher l'état initial.
Pour pouvoir modifier un état après l'affichage d'un composant, il te faudra utiliser le state (tu vas voir ça dans les étapes suivantes)"	https://i.stack.imgur.com/wqvF2.png		
"Dans le composant Todo, utiliser useState pour créer un état ""done"" et son modificateur ""setDone"", avec comme valeur par défaut la propriété ""completed"" issue des props.
Dans le JSX, appeler le state ""done"" à la place de la propriété ""completed"" des props. Vérifier que rien ne change à l'affichage."	https://fr.reactjs.org/docs/hooks-reference.html#usestate		
"Dans le composant Todo, ajouter une balise BUTTON dans la DIV de classe ""Todo"".
Si le state ""done"" des props est vrai, afficher le texte ""❌"", sinon afficher le texte ""✅"".

Sur le bouton, ajouter une action au clic qui fera un console.log de l'état ""done"".
Observer la console du navigateur pour voir si l'état s'affiche bien."	https://fr.reactjs.org/docs/handling-events.html		
"Modifier l'action au clic du bouton pour modifier l'état ""done"" et lui donner l'inverse de sa valeur (s'il est vrai, il passe à faux, et inversement).
Vérifier que l'affichage se modifie bien."	https://dommagnifi.co/2020-12-03-toggle-state-with-react-hooks/		
"Dans App, ajouter une balise BUTTON dans la DIV de class ""App"".
Ajouter une action au clic appelant une fonction ""loadTodos"".

Installer le module axios et importez-le dans le composant App.

Dans la fonction ""loadTodos"", faire un appel à axios pour charger le contenu de l'API suivante : 
https://jsonplaceholder.typicode.com/todos/

Faire un console.log du contenu de la réponse et prendre le temps de l'étudier (que contient la réponse, que contient sa propriété data ?)."	https://jsonplaceholder.typicode.com/todos/		
"Dans App, créer un state ""todos"" avec son modificateur ""setTodos"", initialisé à un tableau vide.

Lors de la récupération de la réponse de l'API, modifier l'état de ""todos"" par le contenu du tableau de résultat de la réponse.

Dans le JSX, remplacer l'appel de ""todoList"" par ""todos"" et vérifier qu'une liste de composant s'affiche bien.

Quand c'est le cas, supprimer le tableau ""todoList"" du composant."			
Ajouter trois boutons "Show all", "Show to do" et "Show done", qui permettront de filtrer la liste pour n'avoir que les éléments sur leur propriété" "completed"	https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/filter		